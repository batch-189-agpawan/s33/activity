// alert('hi')
//retrieve all the to do list items
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => console.log(data))

//array returning just the title of every item
fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((data) => {
	console.log(data.map(toDo => {
		return toDo.title;
	}));
});


//retrieve a single to do list item
fetch('https://jsonplaceholder.typicode.com/todos/2')
.then((response) => response.json())
.then((data) => console.log(data))

//print message in console providing title and status
fetch('https://jsonplaceholder.typicode.com/todos/2')
.then((response) => response.json())
.then((data) => 
	{console.log(data.title)
	console.log(data.completed)});




//fetch request using POST to create a to do list
fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'I am the new post'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


//update a to do list item
fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated to do list'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))


//update to do list by changing the data structure
let today = new Date();
let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
let dateTime = date+' '+time;


fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'number 9',
		description: 'this is the description',
		status: 'Completed',
		dateCompleted: `${dateTime}`,
		userId : 4

	})
})
.then((response) => response.json())
.then((data) => console.log(data))


//fetch request using PATCH method
fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'this item has been patched'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

//Delete
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'DELETE'
})
.then((response) => response.json())
.then((data) => console.log(data))